const fs = require("fs");
const fsP = require("fs/promises");
const Minio = require("minio");
const tmpP = require("tmp-promise");
const util = require("util");
const yauzl = require("yauzl");
const yazl = require("yazl");

const sources = [["world", ""], ["world_nether", "DIM-1/"], ["world_the_end", "DIM1/"]];

const S3_ENDPOINT = process.argv[2];
const S3_BUCKET = process.argv[3];
const S3_ACCESS_KEY = process.argv[4];
const S3_SECRET_KEY = process.argv[5];
const DEST = process.argv[6];

const s3Client = new Minio.Client({
	endPoint: S3_ENDPOINT,
	accessKey: S3_ACCESS_KEY,
	secretKey: S3_SECRET_KEY,
	useSSL: true,
});

(async () => {
	await tmpP.withFile(async (outFile) => {
		const contents = await new Promise((resolve, reject) => {
			const output = [];
			const stream = s3Client.listObjects(S3_BUCKET, undefined, true);
			stream.on("data", obj => output.push(obj));
			stream.on("end", () => resolve(output));
			stream.on("error", reject);
		});
		console.log(contents);

		contents.sort((a, b) => -a.name.localeCompare(b.name));

		console.log("runTask");
		const dest = new yazl.ZipFile();
		const pipe = dest.outputStream.pipe(fs.createWriteStream(outFile.path));
		for(let i = 0; i < sources.length; i++) {
			const srcPath = getPath(contents.find(x => x.name.startsWith(sources[i][0] + "/")));
			await tmpP.withFile(async (inFile) => {
				const srcStream = await s3Client.getObject(S3_BUCKET, srcPath);
				await new Promise((resolve, reject) => {
					console.log("saving", srcPath, "to", inFile.path);
					srcStream
						.pipe(fs.createWriteStream(inFile.path))
						.on("finish", resolve)
						.on("error", reject);
				});
				console.log("saved file");
				const src = await util.promisify(yauzl.open)(inFile.path, {lazyEntries: true});
				src.readEntry();
				await new Promise((resolve, reject) => {
					src.on("entry", entry => {
						console.log("entry", entry.fileName);
						if(entry.fileName.startsWith(sources[i][0] + "/" + sources[i][1])) {
							src.openReadStream(entry, (err, readStream) => {
								if(err) reject(err);
								else {
									dest.addReadStream(readStream, "world/" + entry.fileName.substring(sources[i][0].length + 1));
									src.readEntry();
								}
							});
						}
						else {
							src.readEntry();
						}
					});
					src.on("end", resolve);
					src.on("error", reject);
				});
			});
		}

		console.log("finishing");

		dest.end();

		await new Promise((resolve, reject) => {
			pipe.on("close", resolve);
			pipe.on("error", reject);
		});

		await fsP.chmod(outFile.path, 0o755);
		await fsP.copyFile(outFile.path, DEST);
	});
	console.log("done");
})()
	.then(() => {
		console.log("ended");
	})
	.catch(err => {
		console.error(err);
		process.exit(1);
	});

function getPath(obj) {
	return obj.name;
}
