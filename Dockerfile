FROM node:16-alpine
WORKDIR /usr/src/mondumilo
COPY ./ ./
RUN npm install

RUN echo -e "#!/bin/sh\n/usr/local/bin/node /usr/src/mondumilo/index.js \$BACKUP_ENDPOINT \$BACKUP_BUCKET \$BACKUP_ACCESS_KEY \$BACKUP_SECRET_KEY \$BACKUP_DEST" > /usr/local/bin/mondumilo
RUN chmod +x /usr/local/bin/mondumilo

CMD ["mondumilo"]
